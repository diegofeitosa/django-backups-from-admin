# Django Backups From Admin

Projeto para resolver um problema em se criar backup de arquivo a partir do Django Admin

### Observação
O projeto está configurado apenas com o básico de seu funcionamento, já que o propósito do projeto é apenas desenvolver uma solução que permita a réplica de um arquivo em específico para um diretório em específico onde será criado um diretório com a data em que a réplica foi efetuada. O projeto acaba por servir de gerenciador de versão e backup do arquivo. Dentro das configurações, está configurado para tanto o projeto quanto o banco de dados em questão utilizarem a Zona de Tempo da cidade de São Paulo, assim como o projeto está configurado para a lingua portuguesa do Brasil.

### Comandos
Comandos para o projeto


```
pip install --upgrade pip
pip install django
python manager.py migrate
python manager.py createsuperuser
python manager.py runserver 0.0.0.0:8000
```

### Docker Compose
Configuração simples para rodar o projeto com Docker Compose


```
version: "3.8"

services:
    django:
        image: python:3
        tty: true
        stdin_open: true
        ports:
            - 8000:8000
        volumes:
            - ./django-backups-from-admin:/home/app

networks:
    app:
```
