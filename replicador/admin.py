import os
from django.contrib import admin
from .models import Replica
from django.utils.html import mark_safe, format_html
from pathlib import Path
from datetime import datetime
from django.conf import settings


class ReplicadorAdmin(admin.ModelAdmin):
    list_display = ("solicitante", "download", "remover", "data", "destino",)

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = ("solicitante", "data", )
        form = super(ReplicadorAdmin, self).get_form(request, obj, **kwargs)
        return form

    def download(self, obj):
        destino = settings.STATIC_URL + obj.data.strftime("%Y-%m-%d-%H-%M") + "/" + obj.arquivo
        return mark_safe('<a href="' + destino + '" download>Baixar Arquivo</a>')

    def remover(self, obj):
        return format_html('<a class="btn" href="' + str(obj.id) + '/delete/">Apagar Arquivo</a>')

    def save_model(self, request, obj, form, change):
        now = datetime.now()
        origem = obj.origem + obj.arquivo
        destino = obj.destino + now.strftime("%Y-%m-%d-%H-%M")
        Path(destino).mkdir(exist_ok=True)
        os.system("cp -r -f " + origem + " " + destino)
        obj.solicitante = request.user
        obj.data = now.strftime("%Y-%m-%d %H:%M:%S")
        obj.destino = destino
        super(ReplicadorAdmin, self).save_model(request, obj, form, change)

    def delete_model(self, request, obj):
        os.system("rm -r -f " + obj.destino)
        super(ReplicadorAdmin, self).delete_model(request, obj)

admin.site.register(Replica, ReplicadorAdmin)
