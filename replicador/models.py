from django.db import models
from django.contrib.auth.models import User


class Replica(models.Model):
    solicitante = models.ForeignKey(User, on_delete=models.CASCADE)
    arquivo = models.CharField(max_length=64, default="db.sqlite3")
    origem = models.CharField(max_length=256, default="/home/app/")
    destino = models.CharField(max_length=256, default="/home/app/replicador/static/")
    data = models.DateTimeField(unique=True)

    def __str__(self):
        return self.destino + self.arquivo
